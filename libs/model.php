<?php
class Model{
	function __construct($id){
		$this->id = $id;
	}
	function add(){
		$db = new database;
		$table = $this->table;
		unset($this->id);
		unset($this->table);
		$this->id = $db->insert($table, $this);
		return true;
	}
	function modify(){
		$after = 'WHERE id = ' . $this->id;
		$table = $this->table;
		$db = new database;
		unset($this->id);
		unset($this->table);
		unset($this->db);
		$db->update($table, $this, $after);
		return true;
	}
	function remove(){
		$db = new database;
		$after = 'WHERE id = ' . $this->id;
		$db->delete($this->table, $after);
		return true;
	}
	function get($after = null){
		$db = new database;
		$result = $db->select($this->table, $after);
		return $result;
	}
	function getById(){
		$after = 'WHERE id = ' . $this->id;
		return $this->get($after);
	}
}
?>