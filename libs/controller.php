<?php
class Controller{
	function __construct(){
		$this->view = new view;
		$this->session = new session;
		$this->session->start();
	}
	function welcome(){
		if(HAS_SESSION):
			if($this->session->get(SESSION_KEY)):
				$this->view->render('welcome');
			else:
				$this->view->render('login');
			endif;
		else:
			$this->view->render('welcome');
		endif;
	}
	function error404(){
		$this->view->render('404');
	}
	function go($location){
		header("Location: " . $location);
	}
}
?>