<?php
class Bootstrap{
	function __construct(){
		// Get the url request
		$r = (isset($_GET['r'])) ? $_GET['r'] : null;
		$r = rtrim($r, '/');
		$r = explode('/', $r);

		// If there's nothing after the '/', run welcome method from the controller
		if(empty($r[0])):
			$controller = new controller;
			$controller->welcome();
			return false;
		// Else, try to find the controller
		else:
			$file = CONTROLLERS . $r[0] . '.php'; // Path of the controller based on the first value of the url
			$pageController = CONTROLLERS . 'pager.php';
			// If is set a third value in the url
			if(isset($r[2])):
				// Check if the file of the controller exists first
				if(file_exists($file)):
					// Instance the controller based on the url
					$controller = new $r[0];
					// Check if the method exists based on the second value of the url
					if(method_exists($controller, $r[1])):
						// Call the method based on the url
						$controller->{$r[1]}($r[2]);
					else:
						// If the method doesn't exist, render 404 page
						$controller->error404();
					endif;
				else:
					// If the file of the controller doesn't exist, render 404 page
					$controller = new controller;
					$controller->error404();
				endif;
				// If there's no third value in the url, check it there's a second value
			elseif(isset($r[1])):
				// Check if the file of the controller exists first
				if(file_exists($file)):
					// Instance the controller based on the url
					$controller = new $r[0];
					// Check if the method exists based on the second value of the url
					if(method_exists($controller, $r[1])):
						// Call the method based on the url
						$controller->{$r[1]}();
					else:
						// If the method doesn't exist, render 404 page
						$controller->error404();
					endif;
				else:
					// If the file of the controller doesn't exist, render 404 page
					$controller = new controller;
					$controller->error404();
				endif;
				// If there's no second value in the url, check if the file of the page controller exists first based on the first value of the url
			elseif(file_exists($pageController)):
				// Instance the page controller
				$controller = new page;
				// Check if the pageExists method exists in the page controller
				if(method_exists($controller, 'pageExists')):
					// Check if the page exists based on the first value of the url
					if($controller->pageExists($r[0])):
						// Check if the renderPage method exists on the page controller
						if(method_exists($controller, 'renderPage')):
							// Render the page based on the first value of the url
							$controller->renderPage($r[0]);
						else:
							// If the renderPage method doesn't exist on the page controller, render 404 page
							$controller = new controller;
							$controller->error404();
						endif;
					else:

						/*********** Don't repeat ***********/

						// If the page doesn't exists, try to find a controller based on the first value of the url
						if(file_exists($file)):
							// Instance the controller based on the url
							$controller = new $r[0];
							// Check if the index method exists
							if(method_exists($controller, 'index')):
								// Call the method index, it must exists in every controller created in the app
								$controller->index();
							else:
								// If the method doesn't exist, render 404 page
								$controller->error404();
							endif;
						else:
							// If the file of the controller doesn't exist, render 404 page
							$controller = new controller;
							$controller->error404();
						endif;

						/*********** Don't repeat ***********/

					endif;
				else:
					// If the pageExists method doesn't exist on the page controller, render 404 page
					$controller = new controller;
					$controller->error404();
				endif;
			else:
				/*********** Don't repeat ***********/

				// If the page doesn't exists, try to find a controller based on the first value of the url
				if(file_exists($file)):
					// Instance the controller based on the url
					$controller = new $r[0];
					// Check if the index method exists
					if(method_exists($controller, 'index')):
						// Call the method index, it must exists in every controller created in the app
						$controller->index();
					else:
						// If the method doesn't exist, render 404 page
						$controller->error404();
					endif;
				else:
					// If the file of the controller doesn't exist, render 404 page
					$controller = new controller;
					$controller->error404();
				endif;

				/*********** Don't repeat ***********/
				
			endif;
		endif;
	}
}
?>