<?php

/*******************************
		Configuration
*******************************/

define('URL', 'http://' . $_SERVER['SERVER_NAME'] . rtrim($_SERVER['PHP_SELF'], 'index.php'));

/* Paths */
define('FW', '../cms/');
define('LIBS', FW . 'libs/');
define('LOGS', FW . 'logs/');
define('CONFIG', 'config/');
define('VIEWS', 'views/');
define('PAGES', 'pages/');
define('MODELS', 'models/');
define('CONTROLLERS', 'controllers/');
define('TOOLS', FW . 'tools/');
define('ASSETS', 'assets/');
define('CSS', ASSETS . 'css/');
define('JS', ASSETS . 'js/');
define('IMG', ASSETS . 'img/');

/* Database */

define('HOST', $database['host']);
define('USER', $database['user']);
define('PASS', $database['pass']);
define('DATABASE', $database['database']);

/* ASSETS */

define('CSS_FILES', serialize($assets['css']));
define('JS_FILES', serialize($assets['js']));

/* INFO */

define('TITLE', $info['title']);

/* SESSION */

if(isset($session['has'])):
	define('HAS_SESSION', $session['has']);
else:
	define('HAS_SESSION', false);
endif;

if(isset($session['key'])) define('SESSION_KEY', $session['key']);

?>