<?php
	class Upload{
		public function __construct($file){
			$this->file = $this->getFilesArray($file);
		}

		public function uploadTo($destination){
			if($this->filesAccepted()):
				return $this->uploadFiles($destination);
			else:
				return false;
			endif;
			
		}

		public function setFileTypes($allowedExts){
			$temp = explode(', ', $allowedExts);
			$this->allowedExts = $temp;
		}

		private function getFileExt($filename){
			$temp = explode('.', $filename);
			$this->fileExt = end($temp);
		}

		private function filesAccepted(){
			foreach($this->file as $file):
				$this->getFileExt($file['name']);
				if ((($file['type'] == 'image/gif')		||
					($file['type'] == 'image/jpeg')		||
					($file['type'] == 'image/jpg')		||
					($file['type'] == 'image/pjpeg')	||
					($file['type'] == 'image/x-png')	||
					($file['type'] == 'image/png'))		&&
					($file['size'] < 10000000)			&&
					in_array($this->fileExt, $this->allowedExts)):
					if($file['error'] > 0):
						return false;
						break;
					else:
						// move_uploaded_file($file['tmp_name'], $this->destination);
						// return $this->destination;
						return true;
					endif;
				else:
					// Not allowed file extension
					// echo "no se va a subir";
					return false;
					break;
				endif;
			endforeach;
		}

		private function uploadFiles($destination){
			$filenames = array();
			$count = 0;
			foreach($this->file as $file):
				$this->destination = $destination . $file['name'];
				move_uploaded_file($file['tmp_name'], $this->destination);
				$filenames[$count] = $this->destination;
				$count++;
			endforeach;
			return $filenames;
		}

		private function getFilesArray($file){
		    $array = array();
		    $count = count($file['name']);
		    $keys = array_keys($file);
		    for ($i=0; $i<$count; $i++) {
		        foreach ($keys as $key) {
		            $array[$i][$key] = $file[$key][$i];
		        }
		    }
		    return $array;
		}
	}
?>