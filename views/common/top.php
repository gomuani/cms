<!doctype html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title><?=TITLE?></title>
	<?php
	$assets['css'] = unserialize(CSS_FILES);
	foreach ($assets['css'] as $file):
		if(strpos($file, 'http://') === 0):
			?>
			<link rel="stylesheet" href="<?=$file?>" type="text/css">
			<?php
		else:
			?>
			<link rel="stylesheet" href="<?=URL . CSS . $file?>.css" type="text/css">
			<?php
		endif;
	endforeach;
	?>
</head>
<body data-url="<?=URL?>">