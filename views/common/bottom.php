<?php
$assets['js'] = unserialize(JS_FILES);
foreach ($assets['js'] as $file):
	if(strpos($file, 'http://') === 0):
		?>
		<script type="text/javascript" src="<?=$file?>"></script>
		<?php
	else:
		?>
		<script type="text/javascript" src="<?=URL . JS . $file?>.js"></script>
		<?php
	endif;
endforeach;
?>

</body>
</html>